# otaqu_test

Flutter project untuk feedback test otaqu.

Mininal 2 page terdiri dari:
- Login page sudah implementasi validation form
- Main Page – menampilkan data dari response API. Data di simpan pada local data persistance sebelum ditampilkan.
- Sudah terkoneksi dengan API. menggunakan package:http/http.dart
- Sudah implementasi local DB. pada projectnya ini local db menggunakan persistance library shared_preferences: ^2.0.11
- Clean code & menggunakan struktur code yang baik. project ini sudah menggunakan cubit state management sebagai controller pada presentation layer. 
domain dan data layer pun memiliki class yg terpisah agar akses service dapat di gunakan di bhalaman-halaman yg membutuhkan.
