import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:otaqu_test/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:otaqu_test/models/user_sessions.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  Future<User> signIn({
    required String email,
    required String password,
  }) async {
    var response = await http.Client().post(
      Uri.parse("http://restapi.adequateshop.com/api/authaccount/login"),
      body: {"email": "testandroid@gmail.com", "password": "qwerty"},
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      encoding: Encoding.getByName('utf-8'),
    );

    log("${response.statusCode} :  ${response.request?.url}",
        name: "Response", error: "${response.body}");

    final user = User.fromJson(jsonDecode(response.body));
    if (response.statusCode == 200) {
      saveUserSessions(user);
      return user;
    } else {
      throw Exception("Unable to perform request!");
    }
  }

  Future<void> saveUserSessions(User user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // Save the user details
    final userMapPref = jsonEncode({
      'token': user.data.token,
      'name': user.data.name,
    });

    await prefs.setString('user_map', userMapPref);
  }

  Future<UserSessions> loadUserSessions() async {
    final prefs = await SharedPreferences.getInstance();
    // Get the language using the key
    if (!prefs.containsKey('user_map')) {
      throw Exception("Unable to perform request!");
    }

    final usermap = prefs.getString('user_map');

    log('loadUserSessions-usermap-services: $usermap');
    final jsonUser = jsonDecode(usermap ?? '');
    log('loadUserSessions-jsonUser-services: $jsonUser');

    return UserSessions.fromJson(jsonUser);
  }
}
