import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otaqu_test/cubit/splash_cubit.dart';
import 'package:provider/src/provider.dart';
import '../../shared/theme.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    context.read<SplashCubit>().loadUserSessions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget header(String name) {
      return Container(
        margin:
            EdgeInsets.only(left: defaultMargin, right: defaultMargin, top: 30),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Howdy,\n${name}',
                    style: blackTextStyle.copyWith(
                        fontSize: 24, fontWeight: semiBold),
                  ),
                  SizedBox(),
                  Text(
                    'Lets Discover a new Adventure!',
                    style:
                        greyTextStyle.copyWith(fontSize: 16, fontWeight: light),
                  ),
                ],
              ),
            ),
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage('assets/image_profile.png'))),
            )
          ],
        ),
      );
    }

    return BlocBuilder<SplashCubit, SplashState>(
      builder: (context, state) {
        if (state is SplashSuccess) {
          return ListView(children: [header(state.user.name)]);
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
