import 'dart:async';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otaqu_test/cubit/auth_cubit.dart';
import 'package:otaqu_test/cubit/splash_cubit.dart';
import 'package:otaqu_test/models/user.dart';
import 'package:otaqu_test/services/auth_services.dart';
import '../../shared/theme.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    context.read<SplashCubit>().loadUserSessions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<SplashCubit, SplashState>(
        listener: (context, state) {
          if (state is SplashSuccess) {
            log('splash-success: $state.user');
            Navigator.pushNamedAndRemoveUntil(
                context, '/main', (route) => false);
          } else if (state is SplashFailed) {
            log('splash-error: $state.error');
            Navigator.pushNamedAndRemoveUntil(
                context, '/get-started', (route) => false);
          }
        },
        builder: (context, state) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 300,
                  height: 300,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'assets/logo-otaqu-big.png',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
