import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int code;
  final String message;
  final UserDetails data;

  User({required this.code, required this.message, required this.data});

  factory User.fromJson(Map<String, dynamic> json) => User(
      code: json['code'],
      message: json['message'],
      data: UserDetails.fromJson(json['data']));

  @override
  List<Object?> get props => [code, message, data];
}

class UserDetails extends Equatable {
  final int id;
  final String name;
  final String email;
  final String token;

  UserDetails(
      {required this.id,
      required this.name,
      required this.email,
      required this.token});

  factory UserDetails.fromJson(Map<String, dynamic> json) => UserDetails(
        id: json['Id'],
        name: json['Name'],
        email: json['Email'],
        token: json['Token'],
      );

  @override
  List<Object?> get props => [id, name, email, token];
}
