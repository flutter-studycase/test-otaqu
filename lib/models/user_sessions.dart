import 'package:equatable/equatable.dart';

class UserSessions extends Equatable {
  final String token;
  final String name;

  UserSessions({required this.token, required this.name});

  factory UserSessions.fromJson(Map<String, dynamic> json) => UserSessions(
        token: json['token'],
        name: json['name'],
      );

  @override
  List<Object?> get props => [token, name];
}
