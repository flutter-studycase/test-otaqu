import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otaqu_test/cubit/splash_cubit.dart';
import 'package:otaqu_test/ui/pages/get_started_page.dart';
import 'package:otaqu_test/ui/pages/main_page.dart';
import 'package:otaqu_test/ui/pages/sign_in_page.dart';
import 'package:otaqu_test/ui/pages/splash_page.dart';

import 'cubit/auth_cubit.dart';
import 'cubit/page_cubit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthCubit(),
        ),
        BlocProvider(create: (context) => PageCubit()),
        BlocProvider(create: (context) => SplashCubit())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (context) => SplashPage(),
          '/get-started': (context) => GetStartedPage(),
          '/sign-in': (context) => SignIn(),
          '/main': (context) => MainPage(),
        },
      ),
    );
  }
}
