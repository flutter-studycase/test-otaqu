import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:otaqu_test/models/user.dart';
import 'package:otaqu_test/models/user_sessions.dart';
import 'package:otaqu_test/services/auth_services.dart';

part 'splash_state.dart';

class SplashCubit extends Cubit<SplashState> {
  SplashCubit() : super(SplashInitial());

  void loadUserSessions() async {
    try {
      emit(SplashLoading());
      //I simulate the process with future delayed for 3 second
      await Future.delayed(Duration(seconds: 3));

      UserSessions user = await AuthService().loadUserSessions();
      log('loadUserSessions-cubit: $user');
      emit(SplashSuccess(user));
    } catch (e) {
      emit(SplashFailed(e.toString()));
    }
  }
}
