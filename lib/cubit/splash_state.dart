part of 'splash_cubit.dart';

abstract class SplashState extends Equatable {
  const SplashState();

  @override
  List<Object> get props => [];
}

class SplashInitial extends SplashState {}

class SplashLoading extends SplashState {}

class SplashSuccess extends SplashState {
  final UserSessions user;

  SplashSuccess(this.user);

  @override
  List<Object> get props => [user];
}

class SplashFailed extends SplashState {
  final String error;

  SplashFailed(this.error);

  @override
  List<Object> get props => [error];
}
