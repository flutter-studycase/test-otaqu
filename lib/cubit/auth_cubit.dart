import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:otaqu_test/models/user.dart';
import 'package:otaqu_test/services/auth_services.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  void signIn({required String email, required String password}) async {
    try {
      emit(AuthLoading());
      User user = await AuthService().signIn(
        email: email,
        password: password,
      );

      log('AuthCubit-user: $user');

      emit(AuthSuccess(user));
    } catch (e) {
      emit(AuthFailed(e.toString()));
    }
  }

  void saveUserSessions(User user) async {
    try {
      emit(AuthLoading());
      await AuthService().saveUserSessions(user);

      emit(AuthSuccess(user));
    } catch (e) {
      emit(AuthFailed(e.toString()));
    }
  }
}
